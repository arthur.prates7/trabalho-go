package main

import (
	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"
	"gitlab.com/arthur.prates7/trabalho-go/middlewares"
	"gitlab.com/arthur.prates7/trabalho-go/routes"
)

func rotaprincipal(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome")
}

func setRoutes(router *mux.Router) {
	router.HandleFunc("/", rotaprincipal)
	router.HandleFunc("/tasks", routes.GetTask)
	router.HandleFunc("/tasks/{taskID}", routes.GetTaskID)
	router.HandleFunc("/newtask", routes.NewTask)
	router.HandleFunc("/tasks/update/{taskID}", routes.UpdateTask)
	router.HandleFunc("/tasks/delete/{taskID}", routes.DeleteTask)

}

func main() {
	var router *mux.Router

	log.Printf("Servidor Online em http://localhost:1616")

	router = mux.NewRouter()

	router.Use(middlewares.JsonMiddleware)

	setRoutes(router)

	err := http.ListenAndServe(":1616", router)
	if err != nil {
		fmt.Println("Error", err)
	}
}
