module gitlab.com/arthur.prates7/trabalho-go

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.8.0
	gitlab.com/baugoncalves/goclass-rest-api v0.0.0-20200917112217-95d9d13d4f16
)
