package routes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"github.com/gorilla/mux"
	"gitlab.com/arthur.prates7/trabalho-go/database"
)

type Task struct {
	ID    int     `json:"id"`
	Descricao  string  `json:"descricao"`
}

var tasks []Task

func GetTask(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET"{
	conn := database.SetConnection()
	defer conn.Close()

	selDB, err := conn.Query("select * from atividade")

	if err != nil {
		fmt.Println("Error to fetch", err)
	}

	for selDB.Next() {
		var task Task

		err = selDB.Scan(&task.ID, &task.Descricao)
		tasks = append(tasks, task)
	}

	encoder := json.NewEncoder(w)
	encoder.Encode(tasks)
}
}



func GetTaskID(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET"{
	conn := database.SetConnection()
	defer conn.Close()
	var task Task

	vars := mux.Vars(r)
	id := vars["taskID"]

	selDB := conn.QueryRow("select * from atividade where id=" + id)

	selDB.Scan(&task.ID, &task.Descricao)

	encoder := json.NewEncoder(w)
	encoder.Encode(task)
	}
}


func NewTask(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST"{
	w.WriteHeader(http.StatusCreated)
	conn := database.SetConnection()
	defer conn.Close()

	var register Task

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request")
	}

	json.Unmarshal(body, &register)

	action, err := conn.Prepare("insert into atividade (descricao) values(?)")
	action.Exec(register.Descricao)

	encoder := json.NewEncoder(w)
	encoder.Encode(register)
}
}


func UpdateTask(w http.ResponseWriter, r *http.Request) {

	if r.Method == "PUT"{
	w.WriteHeader(http.StatusOK)
	conn := database.SetConnection()
	defer conn.Close()

	var register Task

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request")
	}
	vars := mux.Vars(r)
	id := vars["taskID"]

	json.Unmarshal(body, &register)

	action, err := conn.Prepare("Update atividade set descricao=(?) where id="+id)
	action.Exec(register.Descricao)

	encoder := json.NewEncoder(w)
	encoder.Encode(register)
}
}




func DeleteTask(w http.ResponseWriter, r *http.Request) {

	if r.Method == "DELETE"{
	w.WriteHeader(http.StatusOK)
	conn := database.SetConnection()
	defer conn.Close()

	var register Task

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request")
	}
	vars := mux.Vars(r)
	id := vars["taskID"]

	json.Unmarshal(body, &register)

	conn.Exec("Delete from atividade where id="+id)

	fmt.Fprint(w, "Deletado com Sucesso")
}
}





